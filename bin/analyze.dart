#!/usr/bin/env dart

import 'dart:io';

import 'package:sca21/dfa/analyses/udchains/udchains.dart';
import 'package:sca21/dfa/analyses/udchains/printer.dart';
import 'package:sca21/dfa/analyses/anticipability.dart';
import 'package:sca21/dfa/analyses/availability.dart';
import 'package:sca21/dfa/analyses/definitions.dart';
import 'package:sca21/dfa/analyses/constants.dart';
import 'package:sca21/dfa/analyses/dominator.dart';
import 'package:sca21/dfa/analyses/liveness.dart';
import 'package:sca21/types/unifier.dart';
import 'package:sca21/parser/parser.dart';
import 'package:sca21/dfa/analysis.dart';
import 'package:sca21/ast/printer.dart';
import 'package:sca21/cfg/printer.dart';
import 'package:sca21/cfg/builder.dart';
import 'package:sca21/dfa/solver.dart';
import 'package:sca21/util/log.dart';
import 'package:args/args.dart';

ArgResults options;
final argsParser = ArgParser(allowTrailingOptions: true)
  ..addFlag('dominators',
      negatable: false, help: 'Intraprocedural dominator analysis.')
  ..addFlag('definitions',
      negatable: false, help: 'Intraprocedural reaching definitions analysis.')
  ..addFlag('availability',
      negatable: false, help: 'Intraprocedural available expressions analysis.')
  ..addFlag('liveness',
      negatable: false, help: 'Intraprocedural live variables analysis.')
  ..addFlag('anticipability',
      negatable: false, help: 'Intraprocedural very busy expressions analysis.')
  ..addFlag('worklist',
      negatable: false, help: 'Use worklist instead of round-robin solver.')
  ..addFlag('constants',
      negatable: false, help: 'Intraprocedural constant propagation.')
  ..addFlag('udchains',
      negatable: false, help: 'Variable use definition chains.')
  ..addFlag('ast',
      negatable: false, help: 'Generate .dot file with abstract syntax tree.')
  ..addFlag('cfg',
      negatable: false, help: 'Generate .dot files with control flow graphs.')
  ..addFlag('annotate',
      negatable: false, help: 'Data flow annotations in control flow graphs.')
  ..addFlag('verbose',
      negatable: false, help: 'Print warnings and diagnostics to stdout.');

String getUsage() => '''
Usage: dart analyze.dart [options] FILE
Static analysis for a simple imperative language.
Examples:
  dart analyze.dart test.while
Options:
${argsParser.usage}
''';

void fail(String message) {
  stderr.writeln(message);
  exit(1);
}

void main(List<String> args) {
  if (args.isEmpty) {
    fail(getUsage());
  }
  try {
    options = argsParser.parse(args);
  } on FormatException catch (e) {
    fail(e.message);
  }
  if (options.rest.length != 1) {
    fail('Exactly one file should be given.');
  }
  final file = options.rest[0];
  if (File(file).statSync().type != FileSystemEntityType.file) {
    fail('Argument $file is not a file.');
  }
  try {
    if (options['verbose']) initLogger();
    final solver = options['worklist'] ? WorklistSolver() : RoundRobinSolver();
    final typechecker = TypeChecker();
    final parser = Parser();
    log.info('Parsing $file ...');
    final ast = parser.parse(File(file).readAsStringSync());
    if (options['ast']) {
      log.info('Writing abstract syntax tree to .dot file ...');
      File('$file.ast.dot').writeAsStringSync(ASTDotPrinter().print(ast));
      log.info('Generated $file.ast.dot');
    }
    log.info('Typechecking ...');
    typechecker.typecheck(ast);
    log.info('Generating intraprocedural control flow graphs ...');
    final cfgMap = Builder().build(ast);
    for (var function in cfgMap.keys) {
      if (options['cfg']) {
        log.info('Writing control flow graph to .dot file ...');
        File('$file.$function.cfg.dot')
            .writeAsStringSync(CFGDotPrinter().print(cfgMap[function]));
        log.info('Generated $file.$function.cfg.dot');
      }

      void analyze(String option, IntraproceduralFlowAnalysis analysis) {
        log.info('Calculating $option for $function ...');
        final information = solver.solve(analysis);
        if (options['cfg'] && options['annotate']) {
          log.info('Writing annotated control flow graph to .dot file ...');
          File('$file.$function.cfg.$option.dot').writeAsStringSync(
              CFGWithAnnotationsDotPrinter().print(cfgMap[function],
                  annotations: {
                    for (var entry in information.entries)
                      entry.key: '${entry.value}'
                  },
                  annotationDirection:
                      analysis is IntraproceduralBackwardAnalysis
                          ? AnnotationDirection.BACKWARD
                          : AnnotationDirection.FORWARD));
          log.info('Generated $file.$function.cfg.$option.dot');
        }
      }

      if (options['dominators']) {
        analyze('dominators', DominatorAnalysis(cfgMap[function]));
      }
      if (options['definitions']) {
        analyze('definitions', ReachingDefinitionsAnalysis(cfgMap[function]));
      }
      if (options['availability']) {
        analyze('availability', AvailabilityAnalysis(cfgMap[function]));
      }
      if (options['liveness']) {
        analyze('liveness', LivenessAnalysis(cfgMap[function]));
      }
      if (options['anticipability']) {
        analyze('anticipability', AnticipabilityAnalysis(cfgMap[function]));
      }
      if (options['constants']) {
        analyze('constants', ConstantPropagation(cfgMap[function]));
      }
      if (options['udchains']) {
        log.info('Calculating use definition chains for $function ...');
        final udchains = UDChains(cfgMap[function]).chains;
        if (options['cfg'] && options['annotate']) {
          log.info('Writing annotated control flow graph to .dot file ...');
          File('$file.$function.udchains.dot').writeAsStringSync(
              UDChainsDotPrinter().print(cfgMap[function], udchains: udchains));
          log.info('Generated $file.$function.udchains.dot');
        }
      }
    }
    log.info('Pretty formatting $file ...');
    print(Printer().print(ast));
  } on TypeCheckerException {
    fail('Argument $file is not a typesafe .while file');
  } on ParserException {
    fail('Argument $file is not a valid .while file.');
  }
}
