import 'package:sca21/cfg/cfg.dart';

/// A data flow analysis framework for the WHILE language.
abstract class Analysis<T> {
  T get initElement;
  T get oneElement;
  T apply(Node node, T incoming);
  bool equal(T left, T right);
  T merge(T left, T right);
}

abstract class FlowGraph {
  Iterable<Node> get nodes;
  Node get startNode;
  Iterable<Node> outgoing(Node node);
  Iterable<Node> incoming(Node node);
}

abstract class IntraproceduralFlowAnalysis<T> extends Analysis<T>
    with FlowGraph {}

abstract class IntraproceduralForwardAnalysis<T>
    extends IntraproceduralFlowAnalysis<T> {
  IntraproceduralControlFlowGraph cfg;
  IntraproceduralForwardAnalysis(this.cfg);
  @override
  Iterable<Node> get nodes => cfg.nodes;
  @override
  Node get startNode => cfg.entryNode;
  @override
  Iterable<Node> outgoing(Node node) => node.successors;
  @override
  Iterable<Node> incoming(Node node) => node.predecessors;
}

abstract class IntraproceduralBackwardAnalysis<T>
    extends IntraproceduralFlowAnalysis<T> {
  IntraproceduralControlFlowGraph cfg;
  IntraproceduralBackwardAnalysis(this.cfg);
  @override
  Iterable<Node> get nodes => cfg.nodes;
  @override
  Node get startNode => cfg.exitNode;
  @override
  Iterable<Node> outgoing(Node node) => node.predecessors;
  @override
  Iterable<Node> incoming(Node node) => node.successors;
}
