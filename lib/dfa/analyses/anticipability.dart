import 'package:sca21/dfa/analysis.dart';
import 'package:sca21/cfg/printer.dart';
import 'package:sca21/cfg/cfg.dart';

class AnticipabilityAnalysis
    extends IntraproceduralBackwardAnalysis<Set<Expression>> {
  @override
  final initElement = {};
  @override
  final oneElement;
  AnticipabilityAnalysis(IntraproceduralControlFlowGraph cfg)
      : oneElement = cfg.nodes
            .whereType<BinaryOperation>()
            .map((node) => Expression(node))
            .toSet(),
        super(cfg);
  @override
  bool equal(Set<Expression> left, Set<Expression> right) =>
      left.length == right.length && left.every((i) => right.contains(i));
  @override
  Set<Expression> merge(Set<Expression> left, Set<Expression> right) =>
      left.intersection(right);
  @override
  Set<Expression> apply(Node node, Set<Expression> incoming) => incoming
      .difference(node is VariableDefinition
          ? oneElement
              .where((expression) =>
                  expression.node.left == node.variable ||
                  expression.node.right == node.variable)
              .toSet()
          : {})
      .union(node is BinaryOperation ? {Expression(node)} : {});
}

class Expression {
  final BinaryOperation node;
  Expression(this.node);
  @override
  bool operator ==(Object other) =>
      other is Expression && '${this}' == '$other';
  @override
  int get hashCode => '${this}'.hashCode;
  @override
  String toString() => '${node.left}'
      '${operations[node.runtimeType]}'
      '${node.right}';
}
