import 'package:sca21/dfa/analysis.dart';
import 'package:sca21/cfg/cfg.dart';

class ConstantPropagation
    extends IntraproceduralForwardAnalysis<Map<Variable, int>> {
  @override
  final initElement = {};
  @override
  final oneElement = null;

  ConstantPropagation(IntraproceduralControlFlowGraph cfg) : super(cfg);

  @override
  bool equal(Map<Variable, int> left, Map<Variable, int> right) =>
      left == right ||
      (left != oneElement &&
          right != oneElement &&
          left.length == right.length &&
          left.entries.every((entry) =>
              right.containsKey(entry.key) && right[entry.key] == entry.value));

  @override
  Map<Variable, int> merge(Map<Variable, int> left, Map<Variable, int> right) =>
      left == oneElement
          ? right
          : right == oneElement
              ? left
              : Map.fromEntries(left.entries.where((entry) =>
                  right.containsKey(entry.key) &&
                  right[entry.key] == entry.value));

  @override
  Map<Variable, int> apply(Node node, Map<Variable, int> incoming) {
    if (node is VariableDefinition &&
        incoming != oneElement &&
        evaluate(node, incoming) is int) {
      return Map.fromEntries({MapEntry(node.variable, evaluate(node, incoming))}
          .union(incoming.entries
              .where((entry) => entry.key != node.variable)
              .toSet()));
    } else if (node is VariableDefinition &&
        incoming != oneElement &&
        evaluate(node, incoming) is! int) {
      return Map.fromEntries(
          incoming.entries.where((entry) => entry.key != node.variable));
    } else if (node is Assertion &&
        incoming != oneElement &&
        evaluate(node, incoming) == 0) {
      return oneElement;
    } else {
      return incoming;
    }
  }
}

int evaluate(Node node, Map<Variable, int> environment) {
  if (node is BinaryOperation &&
      lookup(node.right, environment) is int &&
      lookup(node.left, environment) is int) {
    final right = lookup(node.right, environment);
    final left = lookup(node.left, environment);
    switch (node.runtimeType) {
      case Add:
        return left + right;
      case Subtract:
        return left - right;
      case Multiply:
        return left * right;
      case Divide:
        return left ~/ right;
      case Less:
        return left < right ? 1 : 0;
      case Greater:
        return left > right ? 1 : 0;
      case Equal:
        return left == right ? 1 : 0;
      case NotEqual:
        return left != right ? 1 : 0;
      default:
        return null;
    }
  } else if (node is UnaryOperation &&
      lookup(node.operand, environment) is int) {
    final value = lookup(node.operand, environment);
    switch (node.runtimeType) {
      case Not:
        return value == 0 ? 1 : 0;
      case Minus:
        return -value;
      default:
        return null;
    }
  } else if (node is FalseBranch && lookup(node.value, environment) is int) {
    return lookup(node.value, environment) == 0 ? 1 : 0;
  } else if (node is TrueBranch && lookup(node.value, environment) is int) {
    return lookup(node.value, environment) != 0 ? 1 : 0;
  } else if (node is SimpleAssignment &&
      lookup(node.value, environment) is int) {
    return lookup(node.value, environment);
  } else {
    return null;
  }
}

int lookup(Operand operand, Map<Variable, int> environment) {
  if (operand is Variable && environment.containsKey(operand)) {
    return environment[operand];
  } else if (operand is Number) {
    return operand.value;
  } else {
    return null;
  }
}
