import 'package:sca21/dfa/analysis.dart';
import 'package:sca21/cfg/visitor.dart';
import 'package:sca21/cfg/cfg.dart';

class LivenessAnalysis extends IntraproceduralBackwardAnalysis<Set<Variable>> {
  final variablesUsesCollector = VariableUsesCollector();
  @override
  final initElement = {};
  @override
  final oneElement = {};
  LivenessAnalysis(IntraproceduralControlFlowGraph cfg) : super(cfg);
  @override
  bool equal(Set<Variable> left, Set<Variable> right) =>
      left.length == right.length && left.every((i) => right.contains(i));
  @override
  Set<Variable> merge(Set<Variable> left, Set<Variable> right) =>
      left.union(right);
  @override
  Set<Variable> apply(Node node, Set<Variable> incoming) => incoming
      .difference(node is VariableDefinition ? {node.variable} : {})
      .union(node.accept(variablesUsesCollector));
}

class VariableUsesCollector extends Visitor<Set<Variable>> {
  @override
  Set<Variable> visitSimpleAssignment(SimpleAssignment node) =>
      visitOperand(node.value);
  @override
  Set<Variable> visitBinaryOperation(BinaryOperation node) =>
      visitOperand(node.left).union(visitOperand(node.right));
  @override
  Set<Variable> visitUnaryOperation(UnaryOperation node) =>
      visitOperand(node.operand);
  @override
  Set<Variable> visitAssertion(Assertion node) => visitOperand(node.value);
  @override
  Set<Variable> visitReturn(Return node) => visitOperand(node.value);
  @override
  Set<Variable> visitBranch(Branch node) => visitOperand(node.value);
  @override
  Set<Variable> visitCall(Call node) => node.arguments.fold(
      {}, (variables, argument) => variables.union(visitOperand(argument)));
  @override
  Set<Variable> visitNode(Node node) => {};
  Set<Variable> visitOperand(Operand operand) =>
      operand is Variable ? {operand} : {};
}
