import 'package:sca21/dfa/analysis.dart';
import 'package:sca21/cfg/cfg.dart';

class ReachingDefinitionsAnalysis
    extends IntraproceduralForwardAnalysis<Set<Definition>> {
  final Set<Definition> nullElement;
  @override
  final initElement = {};
  @override
  final oneElement = {};
  ReachingDefinitionsAnalysis(IntraproceduralControlFlowGraph cfg)
      : nullElement = cfg.nodes
            .whereType<VariableDefinition>()
            .map((node) => Definition(node))
            .toSet(),
        super(cfg);
  @override
  bool equal(Set<Definition> left, Set<Definition> right) =>
      left.length == right.length && left.every((i) => right.contains(i));
  @override
  Set<Definition> merge(Set<Definition> left, Set<Definition> right) =>
      left.union(right);
  @override
  Set<Definition> apply(Node node, Set<Definition> incoming) => incoming
      .difference(node is VariableDefinition
          ? nullElement
              .where((definition) => definition.variable == node.variable)
              .toSet()
          : {})
      .union(node is VariableDefinition ? {Definition(node)} : {});
}

class Definition {
  final VariableDefinition node;
  final Variable variable;
  Definition(this.node) : variable = node.variable;
  @override
  bool operator ==(Object other) =>
      other is Definition && node.id == other.node.id;
  @override
  int get hashCode => node.id.hashCode;
  @override
  String toString() => '($variable,${node.id})';
}
