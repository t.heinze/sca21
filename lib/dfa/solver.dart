import 'package:sca21/cfg/printer.dart';
import 'package:sca21/util/log.dart';
import 'package:sca21/cfg/cfg.dart';
import 'analysis.dart';

abstract class Solver {
  Map<Node, T> solve<T>(IntraproceduralFlowAnalysis<T> analysis);
}

/// A round-robin fixpoint solver for intraprocedural data flow analysis.
class RoundRobinSolver extends Solver {
  @override
  Map<Node, T> solve<T>(IntraproceduralFlowAnalysis<T> analysis) {
    final solution = <Node, T>{};
    log.fine('Data Flow Analysis: Initialization');
    for (var node
        in analysis.nodes.where((node) => node != analysis.startNode)) {
      solution[node] = analysis.oneElement;
      log.fine('Data Flow Analysis: '
          'Fact [[${node.accept(labelPrinter)}]] = ${solution[node]}');
    }
    solution[analysis.startNode] = analysis.initElement;
    log.fine('Data Flow Analysis: '
        'Fact [[${analysis.startNode.accept(labelPrinter)}]] '
        '= ${solution[analysis.startNode]}');
    var stable;
    var i = 1;
    do {
      log.fine('Data Flow Analysis: Step ${i++}');
      stable = true;
      for (var node
          in analysis.nodes.where((node) => node != analysis.startNode)) {
        final information = analysis.apply(
            node,
            analysis.incoming(node).fold(analysis.oneElement,
                (element, other) => analysis.merge(element, solution[other])));
        if (!analysis.equal(solution[node], information)) {
          solution[node] = information;
          stable = false;
        }
        log.fine('Data Flow Analysis: '
            'Fact [[${node.accept(labelPrinter)}]] = ${solution[node]}');
      }
    } while (!stable);
    return solution;
  }
}

/// A worklist fixpoint solver for intraprocedural data flow analysis.
class WorklistSolver extends Solver {
  @override
  Map<Node, T> solve<T>(IntraproceduralFlowAnalysis<T> analysis) {
    final solution = <Node, T>{};
    log.fine('Data Flow Analysis: Initialization');
    final worklist =
        analysis.nodes.where((node) => node != analysis.startNode).toSet();
    for (var node in worklist) {
      solution[node] = analysis.oneElement;
      log.fine('Data Flow Analysis: '
          'Fact [[${node.accept(labelPrinter)}]] = ${solution[node]}');
    }
    solution[analysis.startNode] = analysis.initElement;
    log.fine('Data Flow Analysis: '
        'Fact [[${analysis.startNode.accept(labelPrinter)}]] '
        '= ${solution[analysis.startNode]}');
    var i = 1;
    while (worklist.isNotEmpty) {
      log.fine('Data Flow Analysis: Step ${i++}');
      final node = worklist.first;
      worklist.remove(node);
      final information = analysis.apply(
          node,
          analysis.incoming(node).fold(analysis.oneElement,
              (element, other) => analysis.merge(element, solution[other])));
      if (!analysis.equal(solution[node], information)) {
        solution[node] = information;
        for (var successor in analysis.outgoing(node)) {
          worklist.add(successor);
        }
      }
      log.fine('Data Flow Analysis: '
          'Fact [[${node.accept(labelPrinter)}]] = ${solution[node]}');
    }
    return solution;
  }
}
