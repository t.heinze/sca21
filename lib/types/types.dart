import 'package:sca21/ast/printer.dart';
import 'package:sca21/ast/ast.dart';

abstract class Term {}

class TypeVariable extends Term {
  static final printer = Printer();
  final Node node;
  TypeVariable(this.node);
  @override
  String toString() =>
      node is Expression ? '[[${printer.visitNode(node)}]]' : 'α${node.id}';
}

class IntegerType extends Term {
  IntegerType._();
  static final IntegerType singleton = IntegerType._();
  factory IntegerType() => singleton;
  @override
  String toString() => 'int';
}

class FunctionType extends Term {
  final List<Term> parameterTypes;
  final Term returnType;
  FunctionType(this.parameterTypes, this.returnType);
  @override
  String toString() => '(${parameterTypes.join(',')})->$returnType';
}
