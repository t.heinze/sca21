import 'token.dart';

class Scanner {
  static bool isDigit(String c) =>
      c != null && c.compareTo('0') >= 0 && c.compareTo('9') <= 0;
  static bool isWhiteSpace(String c) =>
      c == ' ' || c == '\t' || c == '\r' || c == '\n';
  static bool isLetter(String c) =>
      (c != null && c.compareTo('a') >= 0 && c.compareTo('z') <= 0) ||
      (c != null && c.compareTo('A') >= 0 && c.compareTo('Z') <= 0);
  static bool isIdentifierStart(String c) => isLetter(c);
  static bool isIdentifierPart(String c) =>
      isIdentifierStart(c) || isDigit(c) || c == '_';

  final String source;
  String tokenValue;
  int cursor;

  Scanner(this.source) : cursor = 0;

  String next() => cursor < source.length ? source[cursor++] : null;
  String peek() => cursor < source.length ? source[cursor] : null;

  Token nextToken() {
    var char = next();
    while (isWhiteSpace(char)) {
      char = next();
    }
    if (isIdentifierStart(char)) {
      tokenValue = char;
      while (isIdentifierPart(peek())) {
        tokenValue += next();
      }
      return lookup(tokenValue);
    } else if (isDigit(char)) {
      tokenValue = char;
      while (isDigit(peek())) {
        tokenValue += next();
      }
      return Token.INTEGER;
    } else if (char == null) {
      return Token.EOS;
    } else {
      switch (char) {
        case '=':
          Token token;
          if (peek() == '=') {
            token = Token.EQ;
            next();
          } else {
            token = Token.ASSIGN;
          }
          return token;
        case '!':
          Token token;
          if (peek() == '=') {
            token = Token.NE;
            next();
          } else {
            token = Token.NOT;
          }
          return token;
        case '(':
          return Token.LPAREN;
        case ')':
          return Token.RPAREN;
        case '{':
          return Token.LBRACE;
        case '}':
          return Token.RBRACE;
        case '+':
          return Token.PLUS;
        case '-':
          return Token.MINUS;
        case '*':
          return Token.STAR;
        case '/':
          return Token.SLASH;
        case '<':
          return Token.LT;
        case '>':
          return Token.GT;
        case ';':
          return Token.SEMICOLON;
        case ',':
          return Token.COMMA;
        default:
          return Token.OTHER;
      }
    }
  }
}
