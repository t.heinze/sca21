import 'package:sca21/ast/ast.dart';
import 'scanner.dart';
import 'token.dart';

class ParserException implements Exception {}

/// A recursive descent parser for the WHILE language.
class Parser {
  Scanner scanner;
  Token token;

  void expect(Token token) {
    if (this.token == token) {
      this.token = scanner.nextToken();
    } else {
      throw ParserException();
    }
  }

  Node parse(String source) {
    scanner = Scanner(source);
    token = scanner.nextToken();
    final node = program();
    if (token != Token.EOS) {
      throw ParserException();
    }
    return node;
  }

  // program ::= function { function }
  Program program() {
    final functions = <FunctionDeclaration>[];
    functions.add(function());
    while (token == Token.IDENTIFIER) {
      functions.add(function());
    }
    return Program(functions);
  }

  // function ::= identifier '(' parameters ')'
  //              '{' { statement } 'return' expression ';' '}'
  FunctionDeclaration function() {
    final statements = <Statement>[];
    Identifier name;
    if (token == Token.IDENTIFIER) {
      name = Identifier(scanner.tokenValue);
      token = scanner.nextToken();
    } else {
      throw ParserException();
    }
    expect(Token.LPAREN);
    final params = parameters();
    expect(Token.RPAREN);
    expect(Token.LBRACE);
    while (token == Token.IF ||
        token == Token.WHILE ||
        token == Token.LBRACE ||
        token == Token.IDENTIFIER ||
        token == Token.MINUS ||
        token == Token.NOT ||
        token == Token.INTEGER ||
        token == Token.LPAREN) {
      statements.add(statement());
    }
    expect(Token.RETURN);
    statements.add(ReturnStatement(expression()));
    expect(Token.SEMICOLON);
    expect(Token.RBRACE);
    return FunctionDeclaration(name, params, Block(statements));
  }

  // parameters ::= [ identifier { ',' identifier } ]
  Parameters parameters() {
    final parameters = <Identifier>[];
    if (token == Token.IDENTIFIER) {
      parameters.add(Identifier(scanner.tokenValue));
      token = scanner.nextToken();
      while (token == Token.COMMA) {
        token = scanner.nextToken();
        if (token == Token.IDENTIFIER) {
          parameters.add(Identifier(scanner.tokenValue));
          token = scanner.nextToken();
        } else {
          throw ParserException();
        }
      }
    }
    return Parameters(parameters);
  }

  // block ::= '{' { statement } '}'
  Block block() {
    final statements = <Statement>[];
    expect(Token.LBRACE);
    while (token == Token.IF ||
        token == Token.WHILE ||
        token == Token.LBRACE ||
        token == Token.IDENTIFIER ||
        token == Token.MINUS ||
        token == Token.NOT ||
        token == Token.INTEGER ||
        token == Token.LPAREN) {
      statements.add(statement());
    }
    expect(Token.RBRACE);
    return Block(statements);
  }

  // statement ::= 'if' '(' expression ')' statement [ 'else' statement ]
  //               | 'while' '(' expression ')' statement
  //               | expression ';'
  //               | block
  Statement statement() {
    Statement node;
    if (token == Token.IF) {
      token = scanner.nextToken();
      expect(Token.LPAREN);
      final condition = expression();
      expect(Token.RPAREN);
      final ifBranch = statement();
      Statement elseBranch;
      if (token == Token.ELSE) {
        token = scanner.nextToken();
        elseBranch = statement();
      }
      node = IfElseStatement(condition, ifBranch, elseBranch);
    } else if (token == Token.WHILE) {
      token = scanner.nextToken();
      expect(Token.LPAREN);
      final condition = expression();
      expect(Token.RPAREN);
      node = WhileStatement(condition, statement());
    } else if (token == Token.IDENTIFIER ||
        token == Token.MINUS ||
        token == Token.NOT ||
        token == Token.INTEGER ||
        token == Token.LPAREN) {
      node = expression();
      expect(Token.SEMICOLON);
    } else if (token == Token.LBRACE) {
      node = block();
    } else {
      throw ParserException();
    }
    return node;
  }

  // expression ::= relation [ '=' expression ]
  Expression expression() {
    var node = relation();
    if (token == Token.ASSIGN) {
      if (node is Identifier) {
        token = scanner.nextToken();
        node = Assignment(node, expression());
      } else {
        throw ParserException();
      }
    }
    return node;
  }

  // relation ::= operation { ('=='|'!='|'<'|'>') operation }
  Expression relation() {
    var node = operation();
    while (token == Token.EQ ||
        token == Token.NE ||
        token == Token.LT ||
        token == Token.GT) {
      if (token == Token.EQ) {
        token = scanner.nextToken();
        node = Equal(node, operation());
      } else if (token == Token.NE) {
        token = scanner.nextToken();
        node = NotEqual(node, operation());
      } else if (token == Token.LT) {
        token = scanner.nextToken();
        node = Less(node, operation());
      } else if (token == Token.GT) {
        token = scanner.nextToken();
        node = Greater(node, operation());
      }
    }
    return node;
  }

  // operation ::= term { ('+'|'-') term }
  Expression operation() {
    var node = term();
    while (token == Token.PLUS || token == Token.MINUS) {
      if (token == Token.PLUS) {
        token = scanner.nextToken();
        node = Add(node, term());
      } else if (token == Token.MINUS) {
        token = scanner.nextToken();
        node = Subtract(node, term());
      }
    }
    return node;
  }

  // term ::= factor { ('*'|'/') factor }
  Expression term() {
    var node = factor();
    while (token == Token.STAR || token == Token.SLASH) {
      if (token == Token.STAR) {
        token = scanner.nextToken();
        node = Multiply(node, factor());
      } else if (token == Token.SLASH) {
        token = scanner.nextToken();
        node = Divide(node, factor());
      }
    }
    return node;
  }

  // factor ::= [ '-' | '!' ] factor | postfix
  Expression factor() {
    Expression node;
    if (token == Token.MINUS) {
      token = scanner.nextToken();
      node = Minus(factor());
    } else if (token == Token.NOT) {
      token = scanner.nextToken();
      node = Not(factor());
    } else {
      node = postfix();
    }
    return node;
  }

  // postfix ::= primary { '(' arguments ')' }
  Expression postfix() {
    var node = primary();
    while (token == Token.LPAREN) {
      token = scanner.nextToken();
      node = Call(node, arguments());
      expect(Token.RPAREN);
    }
    return node;
  }

  // primary ::= identifier | number | '(' expression ')'
  Expression primary() {
    Expression node;
    if (token == Token.IDENTIFIER) {
      node = Identifier(scanner.tokenValue);
      token = scanner.nextToken();
    } else if (token == Token.INTEGER) {
      node = Number(int.parse(scanner.tokenValue));
      token = scanner.nextToken();
    } else if (token == Token.LPAREN) {
      token = scanner.nextToken();
      node = expression();
      expect(Token.RPAREN);
    } else {
      throw ParserException();
    }
    return node;
  }

  // arguments ::= [ expression { ',' expression } ]
  Arguments arguments() {
    final arguments = <Expression>[];
    if (token == Token.IDENTIFIER ||
        token == Token.MINUS ||
        token == Token.NOT ||
        token == Token.INTEGER ||
        token == Token.LPAREN) {
      arguments.add(expression());
      while (token == Token.COMMA) {
        token = scanner.nextToken();
        if (token == Token.IDENTIFIER ||
            token == Token.MINUS ||
            token == Token.NOT ||
            token == Token.INTEGER ||
            token == Token.LPAREN) {
          arguments.add(expression());
        } else {
          throw ParserException();
        }
      }
    }
    return Arguments(arguments);
  }
}
