import 'ast.dart';

abstract class ExpressionVisitor<T> {
  T visitExpression(Expression node) => null;
  T visitAssignment(Assignment node) => visitExpression(node);
  T visitBinaryExpression(BinaryExpression node) => visitExpression(node);
  T visitLess(Less node) => visitBinaryExpression(node);
  T visitGreater(Greater node) => visitBinaryExpression(node);
  T visitEqual(Equal node) => visitBinaryExpression(node);
  T visitNotEqual(NotEqual node) => visitBinaryExpression(node);
  T visitAdd(Add node) => visitBinaryExpression(node);
  T visitSubtract(Subtract node) => visitBinaryExpression(node);
  T visitMultiply(Multiply node) => visitBinaryExpression(node);
  T visitDivide(Divide node) => visitBinaryExpression(node);
  T visitUnaryExpression(UnaryExpression node) => visitExpression(node);
  T visitMinus(Minus node) => visitUnaryExpression(node);
  T visitNot(Not node) => visitUnaryExpression(node);
  T visitCall(Call node) => visitExpression(node);
  T visitNumber(Number node) => visitExpression(node);
  T visitAssignableExpression(AssignableExpression node) =>
      visitExpression(node);
  T visitIdentifier(Identifier node) => visitAssignableExpression(node);
}

abstract class Visitor<T> extends ExpressionVisitor<T> {
  T visitNode(Node node) => null;
  T visitProgram(Program node) => visitNode(node);
  T visitFunctionDeclaration(FunctionDeclaration node) => visitNode(node);
  T visitStatement(Statement node) => visitNode(node);
  T visitBlock(Block node) => visitStatement(node);
  T visitIfElseStatement(IfElseStatement node) => visitStatement(node);
  T visitWhileStatement(WhileStatement node) => visitStatement(node);
  T visitReturnStatement(ReturnStatement node) => visitStatement(node);
  T visitParameters(Parameters node) => visitNode(node);
  T visitArguments(Arguments node) => visitNode(node);
}
