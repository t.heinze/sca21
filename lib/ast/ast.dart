import 'visitor.dart';

abstract class Node {
  static int counter = 0;
  final int id;
  Node() : id = counter++;
  T accept<T>(Visitor<T> visitor) => visitor.visitNode(this);
}

class Program extends Node {
  final List<FunctionDeclaration> functions;
  Program(this.functions);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitProgram(this);
}

class FunctionDeclaration extends Node {
  final Parameters parameters;
  final Identifier name;
  final Block body;
  FunctionDeclaration(this.name, this.parameters, this.body);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitFunctionDeclaration(this);
}

abstract class Statement extends Node {
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitStatement(this);
}

class Block extends Statement {
  final List<Statement> statements;
  Block(this.statements);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitBlock(this);
}

class IfElseStatement extends Statement {
  final Statement elseBranch; // maybe null
  final Statement ifBranch;
  final Expression condition;
  IfElseStatement(this.condition, this.ifBranch, this.elseBranch);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitIfElseStatement(this);
}

class WhileStatement extends Statement {
  final Expression condition;
  final Statement body;
  WhileStatement(this.condition, this.body);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitWhileStatement(this);
}

class ReturnStatement extends Statement {
  final Expression value;
  ReturnStatement(this.value);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitReturnStatement(this);
}

abstract class Expression extends Statement {
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitExpression(this);
}

class Assignment extends Expression {
  final AssignableExpression target;
  final Expression source;
  Assignment(this.target, this.source);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitAssignment(this);
}

abstract class BinaryExpression extends Expression {
  final Expression left, right;
  BinaryExpression(this.left, this.right);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) =>
      visitor.visitBinaryExpression(this);
}

class Less extends BinaryExpression {
  Less(Expression left, Expression right) : super(left, right);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitLess(this);
}

class Greater extends BinaryExpression {
  Greater(Expression left, Expression right) : super(left, right);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitGreater(this);
}

class Equal extends BinaryExpression {
  Equal(Expression left, Expression right) : super(left, right);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitEqual(this);
}

class NotEqual extends BinaryExpression {
  NotEqual(Expression left, Expression right) : super(left, right);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitNotEqual(this);
}

class Add extends BinaryExpression {
  Add(Expression left, Expression right) : super(left, right);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitAdd(this);
}

class Subtract extends BinaryExpression {
  Subtract(Expression left, Expression right) : super(left, right);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitSubtract(this);
}

class Multiply extends BinaryExpression {
  Multiply(Expression left, Expression right) : super(left, right);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitMultiply(this);
}

class Divide extends BinaryExpression {
  Divide(Expression left, Expression right) : super(left, right);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitDivide(this);
}

abstract class UnaryExpression extends Expression {
  final Expression operand;
  UnaryExpression(this.operand);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) =>
      visitor.visitUnaryExpression(this);
}

class Minus extends UnaryExpression {
  Minus(Expression operand) : super(operand);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitMinus(this);
}

class Not extends UnaryExpression {
  Not(Expression operand) : super(operand);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitNot(this);
}

class Call extends Expression {
  final Arguments arguments;
  final Expression target;
  Call(this.target, this.arguments);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitCall(this);
}

class Number extends Expression {
  final int value;
  Number(this.value);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitNumber(this);
}

abstract class AssignableExpression extends Expression {
  @override
  T accept<T>(ExpressionVisitor<T> visitor) =>
      visitor.visitAssignableExpression(this);
}

class Identifier extends AssignableExpression {
  final String name;
  Identifier(this.name);
  @override
  T accept<T>(ExpressionVisitor<T> visitor) => visitor.visitIdentifier(this);
}

class Parameters extends Node {
  final List<Identifier> list;
  Parameters(this.list);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitParameters(this);
}

class Arguments extends Node {
  final List<Expression> list;
  Arguments(this.list);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitArguments(this);
}
