import 'visitor.dart';
import 'ast.dart';

class ASTDotPrinter extends Visitor<String> {
  String print(Program ast) => 'digraph {${visitProgram(ast)}}';
  @override
  String visitNode(Node node) =>
      '${node.id}[shape=box,label="${node.runtimeType}"];';
  @override
  String visitProgram(Program node) =>
      node.functions
          .map((func) => func.accept(this) + '${node.id}->${func.id};')
          .join('') +
      visitNode(node);
  @override
  String visitFunctionDeclaration(FunctionDeclaration node) =>
      node.name.accept(this) +
      node.parameters.accept(this) +
      node.body.accept(this) +
      visitNode(node) +
      '${node.id}->${node.name.id};'
          '${node.id}->${node.parameters.id};'
          '${node.id}->${node.body.id};';
  @override
  String visitBlock(Block node) =>
      node.statements
          .map((stmt) => stmt.accept(this) + '${node.id}->${stmt.id};')
          .join('') +
      visitNode(node);
  @override
  String visitIfElseStatement(IfElseStatement node) =>
      node.condition.accept(this) +
      node.ifBranch.accept(this) +
      (node.elseBranch?.accept(this) ?? '') +
      visitNode(node) +
      '${node.id}->${node.condition.id};' +
      '${node.id}->${node.ifBranch.id};' +
      (node.elseBranch != null ? '${node.id}->${node.elseBranch.id};' : '');
  @override
  String visitWhileStatement(WhileStatement node) =>
      node.condition.accept(this) +
      node.body.accept(this) +
      visitNode(node) +
      '${node.id}->${node.condition.id};'
          '${node.id}->${node.body.id};';
  @override
  String visitAssignment(Assignment node) =>
      node.target.accept(this) +
      node.source.accept(this) +
      visitNode(node) +
      '${node.id}->${node.source.id};'
          '${node.id}->${node.target.id};';
  @override
  String visitReturnStatement(ReturnStatement node) =>
      node.value.accept(this) +
      visitNode(node) +
      '${node.id}->${node.value.id};';
  @override
  String visitBinaryExpression(BinaryExpression node) =>
      node.left.accept(this) +
      node.right.accept(this) +
      visitNode(node) +
      '${node.id}->${node.left.id};'
          '${node.id}->${node.right.id};';
  @override
  String visitUnaryExpression(UnaryExpression node) =>
      node.operand.accept(this) +
      visitNode(node) +
      '${node.id}->${node.operand.id};';
  @override
  String visitCall(Call node) =>
      node.target.accept(this) +
      node.arguments.accept(this) +
      visitNode(node) +
      '${node.id}->${node.target.id};'
          '${node.id}->${node.arguments.id};';
  @override
  String visitNumber(Number node) =>
      '${node.id}[shape=box,label="Number\\n\\"${node.value}\\""];';
  @override
  String visitIdentifier(Identifier node) =>
      '${node.id}[shape=box,label="Identifier\\n\\"${node.name}\\""];';
  @override
  String visitParameters(Parameters node) =>
      node.list
          .map((par) => par.accept(this) + '${node.id}->${par.id};')
          .join('') +
      visitNode(node);
  @override
  String visitArguments(Arguments node) =>
      node.list
          .map((arg) => arg.accept(this) + '${node.id}->${arg.id};')
          .join('') +
      visitNode(node);
}

const expressions = {
  Less: '<',
  Greater: '>',
  Equal: '==',
  NotEqual: '!=',
  Add: '+',
  Subtract: '-',
  Multiply: '*',
  Divide: '/',
  Minus: '-',
  Not: '!'
};

class Printer extends Visitor<String> {
  static String indent(int indentation) => '  ' * indentation;

  String statement(Statement stmt) =>
      (stmt is! Block ? indent(indentation) : '') +
      stmt.accept(this) +
      (stmt is ReturnStatement || stmt is Expression ? ';' : '');

  int indentation = 0;

  String print(Program ast) => visitProgram(ast);

  @override
  String visitNode(Node node) => node.accept(this);

  @override
  String visitProgram(Program node) =>
      node.functions.map((func) => func.accept(this)).join('\n');

  @override
  String visitFunctionDeclaration(FunctionDeclaration node) {
    final parameters = node.parameters.accept(this);
    final name = node.name.accept(this);
    indentation++;
    final body = statement(node.body);
    indentation--;
    return '$name($parameters) {' +
        (node.body.statements.isNotEmpty ? '\n$body\n}' : '}');
  }

  @override
  String visitBlock(Block node) =>
      node.statements.map((stmt) => statement(stmt)).join('\n');

  @override
  String visitIfElseStatement(IfElseStatement node) {
    final condition = node.condition.accept(this);
    indentation++;
    final ifBranch = statement(node.ifBranch);
    final elseBranch =
        node.elseBranch != null ? statement(node.elseBranch) : null;
    indentation--;
    return 'if ($condition) {' +
        ((node.ifBranch is! Block ||
                (node.ifBranch as Block).statements.isNotEmpty)
            ? '\n$ifBranch\n${indent(indentation)}}'
            : '}') +
        (elseBranch != null
            ? ' else {' +
                ((node.elseBranch is! Block ||
                        (node.elseBranch as Block).statements.isNotEmpty)
                    ? '\n$elseBranch\n${indent(indentation)}}'
                    : '}')
            : '');
  }

  @override
  String visitWhileStatement(WhileStatement node) {
    final condition = node.condition.accept(this);
    indentation++;
    final body = statement(node.body);
    indentation--;
    return 'while ($condition) {' +
        ((node.body is! Block || (node.body as Block).statements.isNotEmpty)
            ? '\n$body\n${indent(indentation)}}'
            : '}');
  }

  @override
  String visitReturnStatement(ReturnStatement node) =>
      'return ${node.value.accept(this)}';

  @override
  String visitAssignment(Assignment node) {
    final source = node.source.accept(this);
    final target = node.target.accept(this);
    return '$target = $source';
  }

  @override
  String visitBinaryExpression(BinaryExpression node) {
    final right =
        node.right is BinaryExpression || node.right is UnaryExpression
            ? '(${node.right.accept(this)})'
            : node.right.accept(this);
    final left = node.left is BinaryExpression || node.left is UnaryExpression
        ? '(${node.left.accept(this)})'
        : node.left.accept(this);
    return '$left${expressions[node.runtimeType]}$right';
  }

  @override
  String visitUnaryExpression(UnaryExpression node) {
    final operand =
        node.operand is BinaryExpression || node.operand is UnaryExpression
            ? '(${node.operand.accept(this)})'
            : node.operand.accept(this);
    return '${expressions[node.runtimeType]}$operand';
  }

  @override
  String visitCall(Call node) {
    return '${node.target.accept(this)}(${node.arguments.accept(this)})';
  }

  @override
  String visitNumber(Number node) => '${node.value}';
  @override
  String visitIdentifier(Identifier node) => node.name;

  @override
  String visitParameters(Parameters node) =>
      node.list.map((par) => par.accept(this)).join(', ');
  @override
  String visitArguments(Arguments node) =>
      node.list.map((arg) => arg.accept(this)).join(', ');
}
