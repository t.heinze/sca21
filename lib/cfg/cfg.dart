import 'visitor.dart';

abstract class IntraproceduralControlFlowGraph {
  Set<Node> get nodes;
  Node get entryNode;
  Node get exitNode;
}

abstract class Node {
  static var counter = 0;
  final predecessors = <Node>[], successors = <Node>[];
  final int id = counter++;
  @override
  bool operator ==(Object other) => other is Node && id == other.id;
  @override
  int get hashCode => id.hashCode;
  T accept<T>(Visitor<T> visitor) => visitor.visitNode(this);
}

class Entry extends Node {
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitEntry(this);
}

class Exit extends Node {
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitExit(this);
}

abstract class Assertion extends Node {
  Operand value;
  Assertion(this.value);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitAssertion(this);
}

class TrueBranch extends Assertion {
  TrueBranch(Operand value) : super(value);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitTrueBranch(this);
}

class FalseBranch extends Assertion {
  FalseBranch(Operand value) : super(value);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitFalseBranch(this);
}

class Return extends Node {
  Operand value;
  Return(this.value);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitReturn(this);
}

class Branch extends Node {
  Operand value;
  Branch(this.value);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitBranch(this);
}

class VariableDefinition extends Node {
  final Variable variable;
  VariableDefinition(this.variable);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitVariableDefinition(this);
}

class SimpleAssignment extends VariableDefinition {
  Operand value;
  SimpleAssignment(Variable variable, this.value) : super(variable);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitSimpleAssignment(this);
}

class Call extends VariableDefinition {
  List<Operand> arguments;
  Operand target;
  Call(Variable variable, this.target, this.arguments) : super(variable);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitCall(this);
}

abstract class BinaryOperation extends VariableDefinition {
  Operand left, right;
  BinaryOperation(Variable variable, this.left, this.right) : super(variable);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitBinaryOperation(this);
}

class Less extends BinaryOperation {
  Less(Variable variable, Operand left, Operand right)
      : super(variable, left, right);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitLess(this);
}

class Greater extends BinaryOperation {
  Greater(Variable variable, Operand left, Operand right)
      : super(variable, left, right);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitGreater(this);
}

class Equal extends BinaryOperation {
  Equal(Variable variable, Operand left, Operand right)
      : super(variable, left, right);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitEqual(this);
}

class NotEqual extends BinaryOperation {
  NotEqual(Variable variable, Operand left, Operand right)
      : super(variable, left, right);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitNotEqual(this);
}

class Add extends BinaryOperation {
  Add(Variable variable, Operand left, Operand right)
      : super(variable, left, right);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitAdd(this);
}

class Subtract extends BinaryOperation {
  Subtract(Variable variable, Operand left, Operand right)
      : super(variable, left, right);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitSubtract(this);
}

class Multiply extends BinaryOperation {
  Multiply(Variable variable, Operand left, Operand right)
      : super(variable, left, right);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitMultiply(this);
}

class Divide extends BinaryOperation {
  Divide(Variable variable, Operand left, Operand right)
      : super(variable, left, right);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitDivide(this);
}

abstract class UnaryOperation extends VariableDefinition {
  Operand operand;
  UnaryOperation(Variable variable, this.operand) : super(variable);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitUnaryOperation(this);
}

class Minus extends UnaryOperation {
  Minus(Variable variable, Operand operand) : super(variable, operand);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitMinus(this);
}

class Not extends UnaryOperation {
  Not(Variable variable, Operand operand) : super(variable, operand);
  @override
  T accept<T>(Visitor<T> visitor) => visitor.visitNot(this);
}

abstract class Operand {}

class Number extends Operand {
  final int value;
  Number(this.value);
  @override
  bool operator ==(Object other) => other is Number && value == other.value;
  @override
  int get hashCode => value.hashCode;
  @override
  String toString() => '$value';
}

class Variable extends Operand {
  final String name;
  Variable(this.name);
  @override
  bool operator ==(Object other) => other is Variable && name == other.name;
  @override
  int get hashCode => name.hashCode;
  @override
  String toString() => name;
}

class VirtualVariable extends Variable {
  static var counter = 1;
  VirtualVariable() : super('_t${counter++}');
}
