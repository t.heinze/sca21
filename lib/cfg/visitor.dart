import 'cfg.dart';

abstract class Visitor<T> {
  T visitNode(Node node) => null;
  T visitEntry(Entry node) => visitNode(node);
  T visitExit(Exit node) => visitNode(node);
  T visitAssertion(Assertion node) => visitNode(node);
  T visitTrueBranch(TrueBranch node) => visitAssertion(node);
  T visitFalseBranch(FalseBranch node) => visitAssertion(node);
  T visitReturn(Return node) => visitNode(node);
  T visitBranch(Branch node) => visitNode(node);
  T visitVariableDefinition(VariableDefinition node) => visitNode(node);
  T visitSimpleAssignment(SimpleAssignment node) =>
      visitVariableDefinition(node);
  T visitCall(Call node) => visitVariableDefinition(node);
  T visitBinaryOperation(BinaryOperation node) => visitVariableDefinition(node);
  T visitLess(Less node) => visitBinaryOperation(node);
  T visitGreater(Greater node) => visitBinaryOperation(node);
  T visitEqual(Equal node) => visitBinaryOperation(node);
  T visitNotEqual(NotEqual node) => visitBinaryOperation(node);
  T visitAdd(Add node) => visitBinaryOperation(node);
  T visitSubtract(Subtract node) => visitBinaryOperation(node);
  T visitMultiply(Multiply node) => visitBinaryOperation(node);
  T visitDivide(Divide node) => visitBinaryOperation(node);
  T visitUnaryOperation(UnaryOperation node) => visitNode(node);
  T visitMinus(Minus node) => visitUnaryOperation(node);
  T visitNot(Not node) => visitUnaryOperation(node);
}
