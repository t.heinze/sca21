import 'visitor.dart';
import 'cfg.dart';

const operations = {
  Less: '<',
  Greater: '>',
  Equal: '==',
  NotEqual: '!=',
  Add: '+',
  Subtract: '-',
  Multiply: '*',
  Divide: '/',
  Minus: '-',
  Not: '!'
};

class LabelPrinter extends Visitor<String> {
  @override
  String visitBinaryOperation(BinaryOperation node) =>
      '${node.id}: ${node.variable} = '
      '${node.left} ${operations[node.runtimeType]} ${node.right}';
  @override
  String visitUnaryOperation(UnaryOperation node) =>
      '${node.id}: ${node.variable} = '
      '${operations[node.runtimeType]}${node.operand}';
  @override
  String visitSimpleAssignment(SimpleAssignment node) => '${node.id}:'
      ' ${node.variable} = ${node.value}';
  @override
  String visitReturn(Return node) => '${node.id}: return ${node.value}';
  @override
  String visitBranch(Branch node) => '${node.id}: ?(${node.value})';
  @override
  String visitTrueBranch(TrueBranch node) => '${node.id}: assert ${node.value}';
  @override
  String visitFalseBranch(FalseBranch node) =>
      '${node.id}: assert !(${node.value})';
  @override
  String visitCall(Call node) => '${node.id}: ${node.variable} = '
      '${node.target}(${node.arguments.map((argument) => argument).join(',')})';
  @override
  String visitEntry(Entry node) => 'entry';
  @override
  String visitExit(Exit node) => 'exit';
}

final labelPrinter = LabelPrinter();

class CFGDotPrinter extends Visitor<String> {
  Set<Node> visitedNodes;

  String print(IntraproceduralControlFlowGraph cfg) {
    visitedNodes = {};
    return 'digraph {${cfg.entryNode.accept(this)}}';
  }

  @override
  String visitNode(Node node) {
    visitedNodes.add(node);
    final label = node.accept(labelPrinter);
    final postfix = node.successors
        .map((succ) => '${node.id}->${succ.id};'
            '${!visitedNodes.contains(succ) ? succ.accept(this) : ''}')
        .join('');
    return '${node.id}[shape=box,label="$label"];$postfix';
  }
}

enum AnnotationDirection { FORWARD, BACKWARD }

class CFGWithAnnotationsDotPrinter extends CFGDotPrinter {
  AnnotationDirection direction;
  Map<Node, String> annotations;

  @override
  String print(IntraproceduralControlFlowGraph cfg,
      {Map<Node, String> annotations = const {},
      AnnotationDirection annotationDirection = AnnotationDirection.FORWARD}) {
    direction = annotationDirection;
    this.annotations = annotations;
    return super.print(cfg);
  }

  @override
  String visitNode(Node node) {
    visitedNodes.add(node);
    final label = node.accept(labelPrinter);
    final postfix = node.successors
        .map((succ) =>
            '${node.id}->${succ.id}' +
            (annotations.containsKey(node) &&
                    direction == AnnotationDirection.FORWARD
                ? '[label="' + annotations[node] + '",fontcolor=red];'
                : annotations.containsKey(succ) &&
                        direction == AnnotationDirection.BACKWARD
                    ? '[label="' + annotations[succ] + '",fontcolor=red];'
                    : ';') +
            '${!visitedNodes.contains(succ) ? succ.accept(this) : ''}')
        .join('');
    return '${node.id}[shape=box,label="$label"];$postfix';
  }
}
