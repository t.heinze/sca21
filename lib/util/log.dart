import 'dart:io';

import 'package:ansicolor/ansicolor.dart';
import 'package:logging/logging.dart';
export 'package:logging/logging.dart';

final log = Logger('sca21');

void initLogger() {
  hierarchicalLoggingEnabled = true;
  log.level = Level.ALL;
  log.onRecord.listen((LogRecord rec) {
    stdout.writeln('[${color(rec.level)(rec.level.name)}]: ${rec.message}');
  });
}

AnsiPen color(Level level) {
  if (level.value <= Level.FINE.value) {
    return AnsiPen()..gray(level: 0.3);
  } else if (level.value <= Level.CONFIG.value) {
    return AnsiPen()..white();
  } else if (level.value <= Level.INFO.value) {
    return AnsiPen()..blue();
  } else if (level.value <= Level.WARNING.value) {
    return AnsiPen()..rgb(r: 1.0, g: 0.5, b: 0.0);
  } else {
    return AnsiPen()..red(bold: true);
  }
}
