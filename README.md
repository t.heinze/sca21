Static analysis for a simple imperative language, inspired by
[TIP](https://github.com/cs-au-dk/TIP).

## Installing

Prerequisites:

 * Dart 2.10 (or later) -- https://www.dartlang.org/

We use [graphviz](https://www.graphviz.org/) for visualization of
analysis artifacts.

If you want to try out the analysis or examine its source, clone
the repository like this:

    $ git clone https://gitlab.com/t.heinze/sca21.git
    $ cd sca21/
    $ dart pub get

## Usage

Here's a simple example of using the analysis on the command line:

    $ dart bin/analyze.dart examples/example.while

This command just formats the `example.while` file and writes the result to
standard output. For more advanced analyses, run the command without any
arguments to see all possible options. Option `--verbose` is recommended
when developing and testing analyses.

To generate abstract syntax trees and control flow graphs use the following
command line options:

    $ dart bin/analyze.dart --ast --cfg examples/example.while

which will build .dot files for the respective intermediate representations,
from which you can generate various image file formats, e.g., using:

    $ dot -Tpng examples/example.while.ast.dot > ast.png
