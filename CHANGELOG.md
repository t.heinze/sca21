# 0.0.1

- initial version

# 0.0.2

- added pretty printer

# 0.0.3

- relaxed syntax rules
- added typechecker

# 0.0.4

- added intraprocedural data flow analysis

# 0.0.5

- added worklist fixpoint solver
- added constant propagation
- added branch assertions
- removed boolean type

# 0.0.6

- added static single assignment form
- added sparse constant propagation

# 0.0.7

- minor refactoring

# 0.1.1.1

- winter term 2021/22 release

# 0.1.1.2

- added typechecker

# 0.1.1.3

- added data flow analysis